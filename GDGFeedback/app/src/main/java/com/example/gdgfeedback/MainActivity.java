package com.example.gdgfeedback;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    EditText name;
    Button submitBtn;
    RatingBar rb;
    Spinner qualifinationSpn;
    RadioButton studentRb, profRb;
    EditText suggestionET;
    SeekBar ageSB;
    CheckBox agreeSB
;    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        submitBtn = (Button)findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb = (RatingBar)findViewById(R.id.ratingBar);
                qualifinationSpn = (Spinner)findViewById(R.id.qualificationSpinner);
                studentRb = (RadioButton)findViewById(R.id.studentRB);
                profRb = (RadioButton)findViewById(R.id.professionalRB);
                suggestionET = (EditText)findViewById(R.id.suggestionET);
                ageSB = (SeekBar)findViewById(R.id.ageSB);
                agreeSB = (CheckBox)findViewById(R.id.agreeCB);
                name = (EditText)findViewById(R.id.nameInput);

                String nameInput = name.getText().toString();
                String suggestion = suggestionET.getText().toString();
                String qualificatiion = qualifinationSpn.getSelectedItem().toString();
                String occupation = null;

                if(studentRb.isChecked()){
                    occupation = "Professional";
                }
                else if(profRb.isChecked()){
                    occupation="Student";
                }
                int age = ageSB.getProgress();
                boolean isAgree = agreeSB.isChecked();
                int rating = rb.getNumStars();
                GDGFeedback formData = new GDGFeedback(nameInput, occupation, rating,qualificatiion, suggestion, age, isAgree);
                Intent i = new Intent(v.getContext(), FeedbackEnd.class);
                i.putExtra("name", nameInput);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
