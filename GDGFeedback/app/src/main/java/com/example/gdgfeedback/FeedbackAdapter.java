package com.example.gdgfeedback;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class FeedbackAdapter extends RecyclerView.Adapter {

    ArrayList<GDGFeedback> gdgFeedbackArrayList;
    Context context;

    public FeedbackAdapter(Context context, ArrayList<GDGFeedback>feedbacklist){
        gdgFeedbackArrayList = feedbacklist;
        this.context = context;
    }

    class feedbackViewHolder extends RecyclerView.ViewHolder{
        TextView name,occupation,qualification,rating;

        public feedbackViewHolder(@NonNull View itemView){
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.nameTV);
            occupation = (TextView)itemView.findViewById(R.id.occupationTV);
            qualification = (TextView)itemView.findViewById(R.id.occupationTV);
            rating = (TextView)itemView.findViewById(R.id.ratingTV);

        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout,parent,false);
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
